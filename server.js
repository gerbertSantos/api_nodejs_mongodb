'use strict'
const app = require('./bin/express.js');
const variables = require('./bin/configuration/variables');

app.listen(3000, () => {
    console.info(`API inicializada com sucesso na porta ${variables.Api.port}`)
});